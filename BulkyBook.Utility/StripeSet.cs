﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkyBook.Utility
{
    public class StripeSet
    {
        public string SecretKey { get; set; }
        public string PublishableKey { get; set; }
    }
}
