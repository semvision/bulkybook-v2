﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkyBook.Utility
{
    public class TwilioSettings
    {
        public string PhoneNo { get; set; }
        public string AuthToken { get; set; }
        public string AccountId { get; set; }
    }
}
